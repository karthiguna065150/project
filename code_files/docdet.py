project_id= 'doc-ai-33' 
location = 'us' # Format is 'us' or 'eu'
processor_id = 'cabc77cd4a5c042b' # Create processor in Cloud Console
file_path = 'Linux for cloud and devops cert.pdf' # The local file in your current working directory

from google.cloud import documentai_v1beta3 as documentai

def process_document_sample(
    project_id: str, location: str, processor_id: str, file_path: str
):
    # Instantiates a client
    opts = {"api_endpoint": f"{location}-documentai.googleapis.com"}
    client = documentai.DocumentProcessorServiceClient(client_options=opts)

    # The full resource name of the processor, e.g.:
    # projects/project-id/locations/location/processor/processor-id
    # You must create new processors in the Cloud Console first
    name = f"projects/{project_id}/locations/{location}/processors/{processor_id}"

    with open(file_path, "rb") as image:
        image_content = image.read()

    # Read the file into memory
    document = {"content": image_content, "mime_type": "application/pdf"}

    # Configure the process request
    request = {"name": name, "document": document}

    # Recognizes text entities in the PDF document
    result = client.process_document(request=request)

    document = result.document

    print("Document processing complete.")

    # For a full list of Document object attributes, please reference this page: https://googleapis.dev/python/documentai/latest/_modules/google/cloud/documentai_v1beta3/types/document.html#Document

    document_pages = document.pages

    # Read the text recognition output from the processor
    text = document.text
    #print("The document contains the following text (first 100 charactes):")
    print(text)

# Run document processing
process_document_sample(project_id, location, processor_id, file_path)
