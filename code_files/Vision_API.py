from __future__ import print_function
from google.cloud import vision
from PIL import Image, ImageDraw
import io

client = vision.ImageAnnotatorClient()
file_name='Test Image.png'
#image_uri = "file://"+file_name
with io.open(file_name,'rb') as image_file:
    content=image_file.read()

image = vision.Image(content=content)
#image.source.image_uri = image_uri

print("Detecting Text")

response = client.text_detection(image=image)
imag = Image.open(file_name)
#print(response)

for text in response.text_annotations:
    #print('=' * 30)
    #print(text.description)
    vects = text.bounding_poly.vertices
    draw = ImageDraw.Draw(imag)
    draw.polygon([
        vects[0].x, vects[0].y,
        vects[1].x, vects[1].y,
        vects[2].x, vects[2].y,
        vects[3].x, vects[3].y], None,'blue')
    imag.save('TEXT_OUTPUT.png', 'PNG')    
    imag = Image.open('TEXT_OUTPUT.png')

print('Saved new image to TEXT_OUTPUT.png')
print('='*30)

print("Detecting Faces")

response = client.face_detection(image=image)
imag = Image.open(file_name)
#print(response)
for face in response.face_annotations:
    #likelihood = vision.Likelihood(face.surprise_likelihood)
    vects = face.bounding_poly.vertices
    #print(vects)
    draw = ImageDraw.Draw(imag)
    draw.polygon([
        vects[0].x, vects[0].y,
        vects[1].x, vects[1].y,
        vects[2].x, vects[2].y,
        vects[3].x, vects[3].y], None,'blue')   
    imag.save('Face_output.png', 'JPEG')
    imag = Image.open('Face_output.png')
print('Saved new image to Face_output.png')
print('=' * 30)
