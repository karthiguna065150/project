from __future__ import print_function
from google.cloud import vision
from PIL import Image, ImageDraw

image_uri = 'gs://visual_api_bucket/eiffel_tower.jpg'

client = vision.ImageAnnotatorClient()
image = vision.Image()
image.source.image_uri = image_uri

response = client.landmark_detection(image=image)

imag = Image.open('gs://visual_api_bucket/eiffel_tower.jpg')
for landmark in response.landmark_annotations:
    #print('=' * 30)
    #print(landmark)
    vects=landmark.bounding_poly.vertices
    draw = ImageDraw.Draw(imag)
    draw.polygon([
        vects[0].x, vects[0].y,
        vects[1].x, vects[1].y,
        vects[2].x, vects[2].y,
        vects[3].x, vects[3].y], None,'blue')
    imag.save('landmark_output.png', 'PNG')
    print('Saved new image to landmark_output.png')
    imag = Image.open('landmark_output.png')
