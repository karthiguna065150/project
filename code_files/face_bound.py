from __future__ import print_function
from google.cloud import vision
from PIL import Image, ImageDraw
uri = 'gs://visual_api_bucket/Test Image.png'

client = vision.ImageAnnotatorClient()
image = vision.Image()
image.source.image_uri = uri
response = client.face_detection(image=image)
#print(response)

imag = Image.open('Test Image.png')
for face in response.face_annotations:
    #likelihood = vision.Likelihood(face.surprise_likelihood)
    vects = face.bounding_poly.vertices
    #print(vects)
    draw = ImageDraw.Draw(imag)
    draw.polygon([
        vects[0].x, vects[0].y,
        vects[1].x, vects[1].y,
        vects[2].x, vects[2].y,
        vects[3].x, vects[3].y], None,'blue')   
    imag.save('output.png', 'JPEG')
    imag = Image.open('output.png')
print('=' * 30)
print('Saved new image to output.jpg')
print('=' * 30)

